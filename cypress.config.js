const { defineConfig } = require('cypress')

module.exports = defineConfig({
  env: {
    url: 'http://localhost:3000/',
  },
  defaultCommandTimeout: 30000,
  reporter: 'cypress-mochawesome-reporter',
  reporterOptions: {
    reportDir: 'cypress/reports',
    charts: true,
    reportPageTitle: 'Conduit Test Report',
    embeddedScreenshots: true,
    inlineAssets: true,
  },
  video: false,
  videoCompression: 32,
  videosFolder: 'cypress/videos',
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
  },
})
