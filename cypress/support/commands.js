// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('enterLoginDetails', (name, email, password) => { 
    cy.get(':nth-child(3) > .nav-link').click()

    cy.get('.btn').should('be.disabled');
    if (name != ''){
      cy.get(':nth-child(1) > .form-control').type(name);
    } 
    if (email != ''){
      cy.get(':nth-child(2) > .form-control').type(email);
    } 
    if (password != ''){
      cy.get(':nth-child(3) > .form-control').type(password);
    } 
 })


 Cypress.Commands.add('verifyEmail', (email) => {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
 })

