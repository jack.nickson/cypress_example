export default class mockIndex {
    static ARTICLES_EMPTY = require("./articles_empty.json")
    static ARTICLES_ONE = require("./articles_one.json")
    static USERS_VALID = require("./userDetails.json")
}
