import LoginPage from './pages/loginPage'
import mockIndex from '../fixtures/mockIndex.js';
import HomePage from './pages/homePage';

const loginPage = new LoginPage();
const homePage = new HomePage();

describe('Tests - Sign up', () => {

    it('Article should display Title, Description, Tags and Favourites', () => {

        // Status Code Example
        cy.intercept('POST', 'https://api.realworld.io/api/users', {
            fixture: "userDetails",
        }).as("signup") 

        cy.visit(Cypress.env('url'))
        cy.get('[data-test="sign-up"]').click()
        cy.enterLoginDetails("nameexample", 'email@email.com', 'passwordexample')

        cy.get('.btn').click();
        cy.wait("@signup");

        homePage.articleTitle().click();

        // TO-DO: Verify Article details are visible
        cy.get('h1').should('be.visible');
        cy.get('.container > .article-meta > .btn-outline-primary').should('be.visible');
        cy.get('.tag-default').should('be.visible');
        cy.get('.col-md-12 > p').should('be.visible');

    })
})




