import LoginPage from './pages/loginPage'
import HomePage from './pages/homePage';

const loginPage = new LoginPage();
const homePage = new HomePage();

describe('Tests - Accessibility', () => {

    beforeEach(() => {
        cy.visit(Cypress.env('url'));
        cy.injectAxe();
    })

    it('Check Accessibility - Home Page', () => {
        cy.checkA11y();
    })

})






