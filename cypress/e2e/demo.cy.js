import LoginPage from './pages/loginPage'
import mockIndex from '../fixtures/mockIndex';
const loginPage = new LoginPage();

describe('Tests - Application Access', () => {
    it('Open Vue App - Intercept API with No Articles', () => {
        cy.intercept('GET', 'https://api.realworld.io/api/articles?limit=10&offset=0', mockIndex.ARTICLES_EMPTY).as("request")
        cy.visit('http://localhost:3000/')
        cy.wait("@request")
        cy.percySnapshot('Example Text')
    })
})



// The below code takes the intercepted JSON with the tag @createProject and ensures it 'deep equals':
//"name": "David",
//"description": "Test Project Description",
//"users": []

/*
cy.get("@createProject").its("request.body").should("deep.equal", {
    "name": "David",
    "description": "Test Project Description",
    "users": []
});
*/


