import LoginPage from './pages/loginPage'
import mockIndex from '../fixtures/mockIndex.js';
import HomePage from './pages/homePage';

const loginPage = new LoginPage();
const homePage = new HomePage();

describe('Tests - Sign up', () => {

    it('Sign up with valid details successfully', () => {

        cy.intercept('POST', 'https://api.realworld.io/api/users', {
            fixture: "userDetails",
        }).as("signup") 

        // - Hardcoded visit method
        cy.visit('http://localhost:3000/')
        // - Locator taken directly from Cypress
        cy.get(':nth-child(3) > .nav-link').click()

        // - Elongated login method would be duplicated
        cy.get('.btn').should('be.disabled');
        cy.get(':nth-child(1) > .form-control').type('name123412');
        cy.get('.btn').should('be.disabled');
        cy.get(':nth-child(2) > .form-control').type('email123412@email.com');
        cy.get('.btn').should('be.disabled');
        cy.get(':nth-child(3) > .form-control').type('password123123124');
        cy.get('.btn').should('be.enabled');

        cy.get('.btn').click();
        cy.wait("@signup");

        cy.get('.banner > .container > .logo-font').should('be.visible');
    })

    it('Attempt Sign-Up with Invalid Password', () => {

        // + Environment variable used in place of the harcoded URL visit
        cy.visit(Cypress.env('url'))

        // + Custom HTML attribute
        cy.get('[data-test="sign-up"]').click()

        // + Custom Cypress command
        cy.enterLoginDetails('nameexample', 'emailexample@email.com', 'pass');

        cy.get('.btn').click();

        // TODO - Assert the popup message that disappears
    })

    it('Attempt Sign-Up with name field left blank', () => {
        cy.visit(Cypress.env('url'))

        // + Now using homepage POM file as well as Custom HTML tag
        homePage.signUpButton().click()

        cy.enterLoginDetails('', 'email@email.com', 'passwordexample')
        cy.get('.btn').should('be.disabled');
    })

    it('Attempt Sign-Up with incorrect email formatting', () => {
        cy.visit(Cypress.env('url'))
        homePage.signUpButton().click();
        cy.enterLoginDetails('nameexample', 'emailincorrectformat', 'passwordexample')
        cy.get('.btn').click();
        
    })


})






