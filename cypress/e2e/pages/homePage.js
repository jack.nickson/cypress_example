class HomePage{

    signUpButton(){
        return cy.get('[data-test="sign-up"]')
    }

    articleTitle(){
        return cy.get(':nth-child(2) > .preview-link > h1')
    }

}
export default HomePage